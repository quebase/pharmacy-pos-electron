const createWindowsInstaller = require('electron-winstaller').createWindowsInstaller;
const path = require('path');

getInstallerConfig()
  .then(createWindowsInstaller)
  .catch((error) => {
    console.error(error.message || error);
    process.exit(1);
  });

function getInstallerConfig () {
  console.log('Creating windows 32 bits installer...');
  return Promise.resolve({
    appDirectory: 'C:\\Dist\\release-builds\\pharmacy-pos-win32-ia32',
    authors: 'Magnum Digital Limited Kenya',
    noMsi: true,
    outputDirectory: 'C:\\Dist\\release-builds\\installers',
    exe: 'Pharmacy-pos.exe',
    setupExe: 'Pharmacy Plus Pos ia32.exe',
    setupIcon: path.join(__dirname, '../assets/images/favicon.ico'),
    loadingGif: path.join(__dirname, '../assets/images/gif/pacman.gif')
  });
}
