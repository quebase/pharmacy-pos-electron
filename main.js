const setupEvents = require('./windows-installer/setup-events');
if (setupEvents.handleSquirrelEvent()) {
    return;
}

const electron = require('electron');
const child = require('child_process').exec;
const {
    app,BrowserWindow
} = electron;

let pathA, pathB, mainWindow, log, arch, cmd, info, os;
const fs = require('fs');

os = require('os');
arch = os.arch();
pathA = 'C:\\Program Files\\Pharmacy Plus POS';
if(fs.existsSync(pathA))
    cmd = '"'+pathA+'\\php\\php" -S localhost:15950 -t "'+pathA+'\\pharmacy-pos" "'+pathA+'\\pharmacy-pos\\router.php"';

pathB = 'C:\\Program Files (x86)\\Pharmacy Plus POS';
if(fs.existsSync(pathB))
    cmd = '"'+pathB+'\\php\\php" -S localhost:15950 -t "'+pathB+'\\pharmacy-pos" "'+pathB+'\\pharmacy-pos\\router.php"';


log = process.env.SystemDrive + process.env.HOMEPATH + '\\Desktop\\POS-logs.txt';

child(cmd, function (err, data) {
    info = err;
    info += data;
    fs.writeFile(log, info, (err) =>{
        if (err) console.log(err);
    });
});

function createWindow () {
    mainWindow = new BrowserWindow({
        width:1200,
        height: 800,
        backgroundColor: '#ffffff',
        webPreferences: {
            nativeWindowOpen: true
        }
    });

    mainWindow.loadURL('http://localhost:15950');

    mainWindow.on('closed', function () {
        mainWindow = null
    });

    mainWindow.webContents.on('new-window', (event, url, frameName, disposition, options, additionalFeatures) => {
        if (frameName === 'print') {
            // open window as modal
            event.preventDefault();
            Object.assign(options, {
                modal: true,
                parent: mainWindow,
                width: options.width,
                height: options.height
            });
            event.newGuest = new BrowserWindow(options)
        }
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    if (mainWindow === null) {
        createWindow()
    }
});
